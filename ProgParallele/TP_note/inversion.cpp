#include <cstdio>
#include <algorithm>
#include <mpi.h>
#include <immintrin.h>

using namespace std;

/**
  * A compiler avec mpic++ inversion.cpp -o inversion -O3 -fopenmp -mavx2
  * A executer avec mpirun -np num-procs ./inversion taille-de-tableau
  * num-procs != 1 execute la fonction inversionMPI
  * num-procs == 1 execute la fonction inversionAVX
  */

/**
  * NE PAS MODIFIER
  * Cette fonction rend l'inverse d'un __m256
  */
__m256 _mm256_invert_ps(__m256 vec)
{
  static __m256i perm = _mm256_set_epi32(0, 1, 2, 3, 4, 5, 6, 7);
  return _mm256_permutevar8x32_ps(vec, perm);
}

void displayVec(float *A, int size)
{
	for (int i=0; i<size; i++)
			cout << A[i] << " ";
	cout << endl;
}

/**
  * Renverser les elements du tableau A alloue de taille size dans le processus 0.
  * Les autres processus appellent la meme fonction mais ont les valeurs initiales A = NULL et size = -1 (a communiquer)
  * Au retour, le tableau A du processus 0 sera surecrit par son inverse
  */
void inversionMPI(float *A, int size)
{
	int procRank, nbProcess;
	float *rbuf;
	float *tabReturn;
	MPI_Comm_rank(MPI_COMM_WORLD, &procRank);
	MPI_Comm_size(MPI_COMM_WORLD, &nbProcess);

	int sizeFrag = size/nbProcess;

	// envoi de la size a tous les processus
	MPI_Bcast(&size, 1, MPI_INT, 0, MPI_COMM_WORLD);

	// envoi du tableau de façon découpé
	rbuf = (float *) malloc(sizeFrag * sizeof(float));
	tabReturn = (float *) malloc(sizeFrag * sizeof(float));
	MPI_Scatter(&A, sizeFrag, MPI_FLOAT, &rbuf, sizeFrag, MPI_FLOAT, 0, MPI_COMM_WORLD);
	
	// retourne le tableau
	for(int i=0; i<sizeFrag; i++)
		tabReturn[i] = rbuf[sizeFrag-i];
	free(tabReturn);

	// autre processus revoit leur partie du tableau au processus 0
	if (procRank != 0)
	{
		MPI_Send(&tabReturn, sizeFrag, MPI_FLOAT, 0, 1, MPI_COMM_WORLD);
	}
	else
	{
		cout << "Les premiers 10 elements avant inversion: ";
		displayVec(A, size);

		float *final;
		final = (float *) malloc(size * sizeof(float));
		// recupere les bouts de tableau de chaque autre processus (id process 0 exclu)
		for(int i=1; i<nbProcess; i++)
			MPI_Recv(&A, sizeFrag, MPI_FLOAT, i, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
	}
}

/** 
  * A compiler avec mpic++ inversion.cpp -o inversion -O3 -fopenmp -mavx2
  * A executer avec mpirun -np 1 ./inversion [taille-de-tableau]
  */
void inversionAVX(float *A, int size)
{
	// A IMPLANTER ...
	cout << "Les premiers 10 elements avant inversion: ";
	displayVec(A, size);

	// declaration du registre pour copier le tab
	__m256 registre1;
	__m256 registre2;
	float* tempTab;

	for(int j=0; j<(size/2)-1; j+=8)		// on decoupe la boucle par le nombre de registre
	{
		// on inverse les registres pour ne pas ecrasser les valeurs entre les deux listes
		registre1 = _mm256_load_ps(&A[j]);
		registre1 = _mm256_invert_ps(registre1);
		
		registre2 = _mm256_load_ps(&A[size-j-8]);
		registre2 = _mm256_invert_ps(registre2);

		// on reecrit le tableau source
		_mm256_store_ps(&A[j], registre2);
		_mm256_store_ps(&A[size-j-8], registre1);
	}
}

void printUsage()
{
  cout << "Utilisation:\n  mpirun -np num-procs ./inversion taille-du-tableau" << endl;
}

/**
  * NE PAS MODIFIER
  */
int main(int argc, char **argv)
{
	MPI_Init(&argc, &argv);
	int procRank, numProc;
	float *A = NULL, size = -1;
	MPI_Comm_rank(MPI_COMM_WORLD, &procRank);
	MPI_Comm_size(MPI_COMM_WORLD, &numProc); 
	if (argc < 2)
	{
		if (procRank == 0) 
			printUsage();
		MPI_Finalize();
		return 0;
	}

	if (procRank == 0) 
	{
		size = atoi(argv[1]);
		A = (float *) _mm_malloc (size * sizeof(float), 32);
		for (int i = 0; i < size; i++)
			A[i] = size - i;
	}

	if (numProc > 1)
	{
		if (procRank == 0)
			cout << "Execution de inversionMPI avec " << numProc << " processus.\n";
		inversionMPI(A, size);
	} 
	else 
	{
		cout << "Execution de inversionAVX.\n";
		inversionAVX(A, size);
	}

	if (procRank == 0)
	{
		int i;
		printf("Les premiers 10 elements apres inversion: ");
		for (i = 0; i < 10; i++) { cout << A[i] << " "; }
		cout << endl;
		for (i = 0; i < size; i++) 
		{
			if (A[i] != i + 1) 
			{ 
				printf("L'element %d du tableau est incorrect apres inversion.\n", i);
				break;
			}
		}
		if (i == size) 
			printf("L'inversion est effectue avec succes.\n");

		free(A);
	}

	MPI_Finalize();
	return 0;
}
