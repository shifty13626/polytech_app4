#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>

#define TAILLE 100
void f(float* a, float* b, int taille, float c)
{
	#pragma parallel omp
	{
		#pragma omp for 
		for(int i = 0; i < taille; i++)
		{
			b[i] = a[i] + c;
			//printf("%f\n", b[i]);
		}
	}
	
}


float somme(float* a, int taille)
{
	float res = 0.0f;
	
	for(int i = 0; i < taille; i++)
	{
		res += a[i];
	}
	
	return res;
}

float sommePara(float* a, int taille)
{
	float res = 0.0f;
	
	#pragma omp parallel for reduction(+:res)
	for(int i = 0; i < taille; i++)
	{
		res += a[i];
	}
	
	return res;
}

int main()
{
	//Exo 1
	//#pragma omp parallel num_threads(6)
	//printf("Hello ,\n");
	//printf("world\n");
	
	// int val;
	// #pragma omp parralel
	// {
		// val = rand();
		// sleep(1);
		// printf("My val is: %d\n", val);
	// }
	
	// float* b = malloc(TAILLE * sizeof(float));
	// int c = 3;
	
	float* a = malloc(TAILLE * sizeof(float));
		
	clock_t t1;
	clock_t t2;
	
	float res = 0.0f;
	float tps = 0.0f;
	
	for(int i = 0; i < TAILLE; i++){
		
		a[i] = i;
	}
	
	t1 = clock();
	res = somme(a, TAILLE);
	t2 = clock();
	tps = (float)(t2 - t1)/CLOCKS_PER_SEC;
	printf("Temps sans parallele: %f s\n", tps);
	
	t1 = clock();
	res = sommePara(a, TAILLE);
	t2 = clock();
	tps = (float)(t2 - t1)/CLOCKS_PER_SEC;
	printf("Temps avec parallele: %f s\n", tps);
	
	
	free(a);
	
	return 0;
}