#include <immintrin.h>
#include <iostream>
#include <chrono>
#include <cstdlib>

#define NREPET 1024

float scalaireNonVecto(float tab0[], float tab1[], int dim);
float scalaireVecto(float tab0[], float tab1[], int dim);

using namespace std;

int main(int argc, char **argv)
{
	int dim = 256;
	float* tab0;
	float* tab1;
	
	// Allouer et initialiser deux tableaux de flottants de taille dim alignes par 32 octets
	// declare
	tab0 = (float*)_mm_malloc(dim*sizeof(float), 32);
	tab1 = (float*)_mm_malloc(dim*sizeof(float), 32);
	// initialize
	for (int i=0; i<dim; i++)
	{
		tab0[i] = i;
		tab1[i] = i+1;
	}

	// calcul scalaire sans vecto
	float resultScalaireNonVecto = scalaireNonVecto(tab0, tab1, dim);

	// calcul scalaire vecto
	float resultScalaireVecto = scalaireVecto(tab0, tab1, dim);

	cout << "scalaire non vecto : " << resultScalaireNonVecto << endl;
	cout << "scalaire vecto : " << resultScalaireVecto << endl;

  return 0;
}

float scalaireNonVecto(float *tab0, float *tab1, int dim)
{
    float result = 0;
    for (int i = 0; i < NREPET; i++) {
		for(int i=0;i<dim;i++){
			result += tab0[i] * tab1[i];
		}
    }
	return result;
}

float scalaireVecto(float *tab0, float *tab1, int dim)
{
    __m256 r2 = _mm256_set1_ps(0);

	for(int i=0; i<dim; i+=8)
	{
		__m256 r0 = _mm256_load_ps(tab0+i);
		__m256 r1 = _mm256_load_ps(tab1+i);
		r2 = _mm256_add_ps(r2, _mm256_mul_ps(r0, r1));

	}

	float resTab[8] __attribute__((aligned(32)));
	_mm256_store_ps(resTab, r2);
	float result = 0;

	for (int i=0; i<8; i++)
		result += resTab[i];
	
	return result;
}