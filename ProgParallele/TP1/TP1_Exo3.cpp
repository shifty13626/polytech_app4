#include <immintrin.h>
#include <iostream>
#include <chrono>
#include <cstdlib>

#define NREPET 1024

float produitScalaireAVX(float *A, float *B, int taille)
{
	
	__m256 res = _mm256_set1_ps(0.0f);
	for (size_t i = 0; i < taille; i += 8){
		
		res = _mm256_fmadd_ps(_mm256_load_ps(A + i), _mm256_load_ps(B + i), res);
	}
	
	float resTab[8] __attribute__((aligned(32)));
	
	_mm256_store_ps(resTab, res);
	
	return resTab[0] + resTab[1] + resTab[2] + resTab[3] + resTab[4] + resTab[5]+ resTab[6] + resTab[7];
	
}

__m128 vect_left1(__m128 r0, mm128 r1)
{
	// r0 = _mm_shuffle_ps(r0, r1 , _MM_SHUFFLE(3, 2, 1, 0)); r1, r1, r0, r0
	__m128 temp;
	
	temp = _mm_shuffle_ps(r0, r1, MM_SHUFFLE(1, 0, 3, 2));
	
	
}

int main(int argc, char **argv)
{
  int dim = std::atoi(argv[1]);
  float* tab0;
  float* tab1;
  float* tab2;
  
  // Allouer et initialiser deux tableaux de flottants de taille dim alignes par 32 octets
  // ...
  tab0 = (float*) _mm_malloc(dim * sizeof(float), 32);
  tab1 = (float*) _mm_malloc(dim * sizeof(float), 32);
  tab2 = (float*) _mm_malloc(8 * sizeof(float), 32);
  
  for(int i = 0; i < dim; i++){
	tab0[i] = i;
	tab1[i] = 0;
  }
  
  for(int i = 0; i < 8; i++){
	tab2[i] = 0;
  }
  
  // Copier tab0 dans tab1 de manière scalaire~(code non-vectorise).
  // On repete NREPET fois pour mieux mesurer le temps d'execution
  auto start = std::chrono::high_resolution_clock::now();
  for (int i = 0; i < NREPET; i++) {
    for(int i=0;i<dim;i++){
      tab1[i]=tab0[i];
    }
  }
  auto end = std::chrono::high_resolution_clock::now();
  std::chrono::duration<double> diffSeq = end-start;
  std::cout << std::scientific << "Copier sans AVX: " << diffSeq.count() / NREPET << "s" << std::endl;

  __m256 ra;
  __m256 rb;
  __m256 rc;
  
  // Copier tab0 dans tab1 de maniere vectorisee avec AVX
  start = std::chrono::high_resolution_clock::now();
  for (int i = 0; i < NREPET; i++) { 
    // faire un mm_load afin de charger 8 floats d'un coup
	// faire un mm_store afin de stocker 8 floats d'un coup
	// i = i + 8 (car on a charger 8 floats)
	for(int i = 0; i<dim; i+=8){
		ra = _mm256_load_ps(&tab0[i]);
		rb = _mm256_load_ps(&tab1[i]);
		
		__m256 temp = _mm256_mul_ps(ra, rb);
		rc = _mm256_add_ps(temp, rc);
		
	}
  }
  
  
  
  
  end = std::chrono::high_resolution_clock::now();
  std::chrono::duration<double> diffPar = end-start;
  std::cout << std::scientific << "Copier avec AVX: " << diffPar.count() / NREPET << "s" << std::endl;

  // Afficher l'acceleration et l'efficacite
  // ...
  double acceleration = diffSeq.count() / diffPar.count();
  double efficacite = acceleration / 8;
  std::cout << "Acceleration: " << acceleration << std::endl;
  std::cout << "Efficacite: " << efficacite << std::endl;

  return 0;
}
