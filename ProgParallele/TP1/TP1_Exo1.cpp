#include <immintrin.h>
#include <iostream>
#include <chrono>
#include <cstdlib>


#define NREPET 1024

int main(int argc, char **argv)
{
	int dim = std::atoi(argv[1]);
	float* tab0;
	float* tab1;
	
	// Allouer et initialiser deux tableaux de flottants de taille dim alignes par 32 octets
	// ...
	tab0 = (float*) _mm_malloc(dim * sizeof(float), 32);
	tab1 = (float*) _mm_malloc(dim * sizeof(float), 32);
	for(int i = 0; i < dim; i++)
	{
		tab0[i] = i;
		tab1[i] = 0;
	}
	
	// Copier tab0 dans tab1 de manière scalaire~(code non-vectorise).
	// On repete NREPET fois pour mieux mesurer le temps d'execution
	auto start = std::chrono::high_resolution_clock::now();
	for (int i = 0; i < NREPET; i++)
	{
		for(int i=0;i<dim;i++)
		{
			tab1[i]=tab0[i];
		}
	}
	auto end = std::chrono::high_resolution_clock::now();
	std::chrono::duration<double> diffSeq = end-start;
	std::cout << std::scientific << "Copier sans AVX: " << diffSeq.count() / NREPET << " s" << std::endl;

	// declaration registre r1
	__m256 r1;
	// Copier tab0 dans tab1 de maniere vectorisee avec AVX
	start = std::chrono::high_resolution_clock::now();
	for (int i = 0; i < NREPET; i++) 
	{ 
		// faire un mm_load afin de charger 8 floats d'un coup
		// faire un mm_store afin de stocker 8 floats d'un coup
		// i = i + 8 (car on a charger 8 floats)
		for(int i = 0; i<dim; i+=8)
		{
			r1 = _mm256_load_ps(&tab0[i]);
			_mm256_store_ps(&tab1[i], r1);
		}
	}
	end = std::chrono::high_resolution_clock::now();
	std::chrono::duration<double> diffPar = end-start;
	std::cout << std::scientific << "Copier avec AVX: " << diffPar.count() / NREPET << " s" << std::endl;

	// Afficher l'acceleration et l'efficacite
	// ...
	double acceleration = diffSeq.count() / diffPar.count();
	double efficacite = acceleration / 8;
	std::cout << "Acceleration: " << acceleration << std::endl;
	std::cout << "Efficacite: " << efficacite << std::endl;

	// liberation des espaces tableau
	_mm_free(tab0);
	_mm_free(tab1);
	return 0;
}
