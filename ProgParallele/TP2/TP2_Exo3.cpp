#include <stdio.h>
#include <omp.h>
#include <iostream>
#include <iomanip>
#include <vector>

using namespace std;

#define SIZE 8000

bool isprime(int x)
{
	for(int i = 2; i * i < x; i++)
	{
		if(x % i == 0){
			return false;
		}
	}
	return true;
}

int goldbach(int x)
{
	int nbPaire = 0; 
	
	#pragma omp parralel for reduction(+=nbPaire)
	for(int i = 0; i < x; i+=2)
	{
		int b = x - i;
		if(isprime(b) && isprime(i)){
			nbPaire++;
		}
	}
	
	return nbPaire;
}

int main()
{
	int x = 13;
	int numpairs[SIZE];
	
	// Init tabl
	#pragma omp parallel for
	for(int i = 0; i < SIZE; i++)
	{
		numpairs[i] = 0;
	}
	
	#pragma omp parallel for
	for(int i = 0; i < SIZE; i+=2)
	{
		numpairs[i] = goldbach(i);
	}
	
	//cout << isprime(x) << endl;
	cout << goldbach(64) << endl;
	
	return 0;
}


