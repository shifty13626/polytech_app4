#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>
#include "omp.h"

int main()
{
	omp_set_num_threads(3);
	#pragma omp parallel
	{
		printf("Thread id: %d\n", omp_get_thread_num());
		
		#pragma omp single
		{
			printf("%d - Hello world\n", omp_get_thread_num());			
		}
	}
	
	return 0;
}