#include<stdio.h>
#include "omp.h"
#include <iostream>

#define nbThread 3

using namespace std;

int main ()
{
    // declare global value
    int initValue = 15;
    int localValue;
    int reducValue = 0;

    // afficher les valeurs
    cout << "Affichage avec private" << endl;
    #pragma omp parallel num_threads(nbThread) private(localValue)
    {
        localValue = 3;
        cout << "valeur global = " << initValue << endl;
        cout << "valeur local = " << localValue << endl;
    }

    cout << "Affichage avec reduction" << endl;
    #pragma omp parralel for reduction(+=reducValue)
    for(int i=0; i<2; i++)
        reducValue ++;

    cout << "reduction value = " << reducValue << endl;

    return 0;
}