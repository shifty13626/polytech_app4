#include <stdio.h>
#include<iostream>

#define MAX 10000

using namespace std;
    
int main()
{
    
    int num = 0;
    
    #pragma omp parallel for reduction(+:num)
    for (int i=0; i<MAX; i++)
        num++;
       
    cout << "n = " << num << endl;
    
    return 0;
}