#include<stdio.h>
#include "omp.h"
#include <iostream>

#define SIZE 256

using namespace std;

int main ()
{
    // declaration tab
    double tab1[SIZE], tab2[SIZE], somme;

    // initialisation tab
    for(int i=0; i<SIZE; i++)
    {
        tab1[i] = i * 0.5;
        tab2[i] = i * 0.25;
    }

    // calcul scalaire
    #pragma omp parallel for reduction(+:somme)
    for(int i=0; i<SIZE; i++)
    {
        somme += tab1[i] + tab2[i];
    }

    cout << "scalaire = " << somme << endl;

    return 0;
}