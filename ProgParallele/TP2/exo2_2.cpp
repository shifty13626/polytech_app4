#include <stdio.h>
#include <omp.h>
#include <iostream>
#include <iomanip>

float f(float x);

using namespace std;

int main()
{
	int n=100, nbThread = 4, idThread;
	double pi = 0;

	double s = (double)1.0 / n;
	cout << "s = " << s << endl;	

	#pragma omp parallel num_threads(nbThread)
	{
		double localPi = 0;
		idThread = omp_get_thread_num();
		for(int i=idThread*n/nbThread; i<(idThread+1)*n/nbThread; i++)
		{
			localPi += s * ((f(i*s) + f((i+1)*s)) / 2);
		}
		cout << localPi << endl;

		#pragma omp critical
		pi += localPi;
	}

	cout << "pi = " << std::setprecision(10) << pi << endl;	

	return 0;
}

float f(float x)
{
	return (4/(1+(x*x)));
}
