<<<<<<< Updated upstream
#include <immintrin.h>
#include <iostream>
#include <chrono>
#include <cstdlib>

#define NREPET 1024

using namespace std;

int main(int argc, char **argv)
{
  int dim = std::atoi(argv[1]);
  float* tab0;
  float* tab1;
  
  // Allouer et initialiser deux tableaux de flottants de taille dim alignes par 32 octets
  // declare
  tab0 = (float*)_mm_malloc(dim*sizeof(float), 32);
  tab1 = (float*)_mm_malloc(dim*sizeof(float), 32);
  // initialize
  for (int i=0; i<dim; i++)
  {
     tab0[i] = i;	
     tab1[i] = 0;
  }
  
  // Copier tab0 dans tab1 de manière scalaire~(code non-vectorise).
  // On repete NREPET fois pour mieux mesurer le temps d'execution
  auto start = std::chrono::high_resolution_clock::now();
  for (int i = 0; i < NREPET; i++) {
    for(int i=0;i<dim;i++){
      tab1[i]=tab0[i];
    }
  }
  auto end = std::chrono::high_resolution_clock::now();
  std::chrono::duration<double> diffSeq = end-start;
  std::cout << std::scientific << "Copier sans AVX: " << diffSeq.count() / NREPET << "s" << std::endl;

  // Copier tab0 dans tab1 de maniere vectorisee avec AVX
  // declaration du registre r1
  __m256 r1;

  start = std::chrono::high_resolution_clock::now();
  for (int i = 0; i < NREPET; i++) {
	// on charge les 8 float d'un coup 32/8 = 4
	for(int i=0; i<dim; i+=8)
	{
	    // copy value in register
	    r1 = _mm256_load_ps(&tab0[i]);
	    // copy value on table
	    _mm256_store_ps(&tab1[i], r1);
	}
  }
  end = std::chrono::high_resolution_clock::now();
  std::chrono::duration<double> diffPar = end-start;
  std::cout << std::scientific << "Copier avec AVX: " << diffPar.count() / NREPET << "s" << std::endl;

  // Afficher l'acceleration et l'efficacite
  // acceleration a = T(1)/T(n)
  float acc = diffSeq / diffPar;
  cout << "acceleration : " << acc << " secondes" << endl;

  //efficacite
  float effic = acc / dim;
  cout << "efficasite : " << effic << endl;

  return 0;
}
=======
#include <immintrin.h>
#include <iostream>
#include <chrono>
#include <cstdlib>

#define NREPET 1024

int main(int argc, char **argv)
{
  int dim = std::atoi(argv[1]);
  float* tab0;
  float* tab1;
  
  // Allouer et initialiser deux tableaux de flottants de taille dim alignes par 32 octets
  // declare
  tab0 = (float*)_mm_malloc(dim*sizeof(float), 32);
  tab1 = (float*)_mm_malloc(dim*sizeof(float), 32);
  // initialize
  for (int i=0; i<dim; i++)
  {
     tab0[i] = i;	
     tab1[i] = 0;
  }
  
  // Copier tab0 dans tab1 de manière scalaire~(code non-vectorise).
  // On repete NREPET fois pour mieux mesurer le temps d'execution
  auto start = std::chrono::high_resolution_clock::now();
  for (int i = 0; i < NREPET; i++) {
    for(int i=0;i<dim;i++){
      tab1[i]=tab0[i];
    }
  }
  auto end = std::chrono::high_resolution_clock::now();
  std::chrono::duration<double> diffSeq = end-start;
  std::cout << std::scientific << "Copier sans AVX: " << diffSeq.count() / NREPET << "s" << std::endl;

  // Copier tab0 dans tab1 de maniere vectorisee avec AVX
  // declaration du registre r1
  __m256 r1;

  start = std::chrono::high_resolution_clock::now();
  for (int i = 0; i < NREPET; i++) {
	// on charge les 8 float d'un coup 32/8 = 4
	for(int i=0; i<dim; i+=8)
	{
	    // copy value in register
	    r1 = _mm256_load_ps(&tab0[i]);
	    // copy value on table
	    _mm256_store_ps(&tab1[i], r1);
	}
  }
  end = std::chrono::high_resolution_clock::now();
  std::chrono::duration<double> diffPar = end-start;
  std::cout << std::scientific << "Copier avec AVX: " << diffPar.count() / NREPET << "s" << std::endl;

  // Afficher l'acceleration et l'efficacite
  // ...

  return 0;
}
>>>>>>> Stashed changes
