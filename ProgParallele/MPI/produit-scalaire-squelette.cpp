#include <iostream>

#include "mpi.h"

using namespace std;

int main(int argc, char **argv)
{
  double *a_global, *a_local;
  double *b_global, *b_local;
  int rank, size;
  int N;
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);
  
  // Initialiser les tableaux a_global et b_global dans le processus 0
  if (rank == 0) {
    N = 32;
    a_global = (double *) malloc(N * sizeof(double));
    b_global = (double *) malloc(N * sizeof(double));
    for (int i = 0; i < N; i++) {
      a_global[i] = i;
      b_global[i] = 1;
    }

    for(int i = 0; i < N; i++){
      cout << a_global[i] << "\t" << b_global[i] << endl;
    }
  }
  // Envoyer la taille de tableau a chaque processus. On suppose que N est divisible par le nombre de processus.
  // A FAIRE ...
  /* if(rank == 0){ // J'envois à tout le monde
     for(int i = 1; i < size; i++){
       MPI_Send(&N, 1, MPI_INT, i, 0, MPI_COMM_WORLD);
       //(var_envoyé, taille, type, vers_le_processus, num_message, groupe)
     }
  }
  else{ // Je recois du processus 0
    MPI_Recv(&N, 1, MP_INT, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    //(var, taille, type, id_expéditeur, id_message, groupe, status)
    cout << "recu: " << N << endl;
  } */

  MPI_Bcast(&N, 1, MPI_INT, 0, MPI_COMM_WORLD);
  //(adr var envoyé, taille, type, source_message, groupe)

  // Allouer a_local et b_local dans chaque processus, puis envoyer la partie correspondante du a_global et b_global du 
  // processus 0 a chaque processus
  // A FAIRE ...
  a_local = (double *) malloc(sizeof(double) * N / size);
  b_local = (double *) malloc(sizeof(double) * N / size);
  
  /* if(rank == 0){
    for(int i = 1; i < size; i++){
      MPI_Send(&a_global[i * N / size], N / size, MPI_DOUBLE, i, 0, MPI_COMM_WORLD);
      MPI_Send(&b_global[i * N / size], N / size, MPI_DOUBLE, i, 1, MPI_COMM_WORLD);
    }

    for(int i = 0; i < N / size; i++){
      a_local[i] = a_global[i];
      b_local[i] = b_global[i];
    }
  }
  else{
    MPI_Recv(&a_local[0], N / size, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    MPI_Recv(&b_local[0], N / size, MPI_DOUBLE, 0, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
  } */

  MPI_Scatter(&a_global[0], N / size, MPI_DOUBLE, &a_local[0], N / size, MPI_DOUBLE, 0, MPI_COMM_WORLD);
  MPI_Scatter(&b_global[0], N / size, MPI_DOUBLE, &b_local[0], N / size, MPI_DOUBLE, 0, MPI_COMM_WORLD);
  
  // Calculer le produit scalaire local dans chaque processus, puis envoyer le resultat au processus 0 pour sommer
  // A FAIRE ...
  double somme = 0.0;

  for(int i = 0; i < N / size; i++){
    somme += a_local[i] * b_local[i];
  }

  double *rbuf;
  if(rank == 0){  
    rbuf = (double *) malloc (size * sizeof(double));
  }

  MPI_Gather(&somme, 1, MPI_DOUBLE, rbuf, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);

  if(rank == 0){
   for(int i = 1; i < size; i++){
     somme += rbuf[i];
   }
  }

  if(rank == 0){
    free(rbuf);
  }
  
  /* if(rank == 0){
    for(int i = 0; i < N / size; i++){
      somme += a_local[i] * b_local[i];
    }

    for(int i = 1; i < size; i++){
      double temp;
      MPI_Recv(&temp, 1, MPI_DOUBLE, i, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
      somme += temp;
    }
  }
  else{
    for(int i = 0; i < N / size; i++){
      somme += a_local[i] * b_local[i];
    }

    MPI_Send(&somme, 1, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD); // on envois la partie calculé
    cout << "rank: " << rank << " send" << endl;
  } */

  // Afficher le resultat du processus 0
  // A FAIRE ...
  if(rank == 0){
    cout << "somme: " << somme << endl;
  }

  // Desallouer a_local et b_local dans chaque processus
  // A FAIRE ...
  free(a_local);
  free(b_local);

  // Desallouer a_global et b_global
  if (rank == 0) {
    free(a_global);
    free(b_global);
  }

  MPI_Finalize();
  return 0;
}
