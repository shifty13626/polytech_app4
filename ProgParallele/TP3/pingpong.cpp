#include<iostream>
#include<mpi.h>

using namespace std;

int main(int argc, char **argv)
{
    int rank, size;

    MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);

    if ((rank % 2) == 0)
    {
        // send id process
        MPI_Send(&rank, 1, MPI_INT, rank+1, 0, MPI_COMM_WORLD);

        // receive id other process
        int id = 0;
        MPI_Recv(&id, 1, MPI_INT, rank+1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        cout << "Id rank : " << rank << " value receive = " << id << endl;
    }
    else
    {
        int id = 0;
        MPI_Recv(&id, 1, MPI_INT, rank-1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

        // send id process
        id = id + 10 * rank;
        MPI_Send(&id, 1, MPI_INT, rank-1, 0, MPI_COMM_WORLD);        
    }
    
    MPI_Finalize();
    return 0;
}