#include <iostream>

#include "mpi.h"

using namespace std;

float f(float x)
{
	return (4/(1+(x*x)));
}

int main(int argc, char **argv)
{
    int rank, size;
    int n = 200;
    double s = (double)1.0 / n;
    double pi = 0.0f;

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
  
    double localPi = 0.0f;

    for(int i = rank * n / size; i<(rank + 1)*n/size;i++){
        localPi += s * ((f(i*s) + f((i+1)*s)) / 2);
    }

    double *rbuf;
    if(rank == 0){  
        rbuf = (double *) malloc (size * sizeof(double));
    }

    MPI_Gather(&localPi, 1, MPI_DOUBLE, rbuf, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);

    if(rank == 0){
        for(int i = 1; i < size; i++){
            localPi += rbuf[i];
        }
    }

    if(rank == 0){
        free(rbuf);
    }

    if(rank == 0){
        cout << "Rank: " << rank << " - PI: " << localPi << endl;
    }

    MPI_Finalize();
    return 0;
}
