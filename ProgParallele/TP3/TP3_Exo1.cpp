#include <iostream>
#include <mpi.h>

using namespace std;

int main(int argc, char **argv)
{
	int rank, size;
	
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	
	if((rank % 2) == 0)		// Nombre pair -> doit envoyer son nombre(id) au processus impair
	{ 
		MPI_Send(&rank, 1, MPI_INT, rank + 1, 0, MPI_COMM_WORLD);

		int var = 0;
		MPI_Recv(&var, 1, MPI_INT, rank + 1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

		cout << "rank: " << rank << " - print: " << var << endl;
	}
	else
	{
		int var = 0;
		MPI_Recv(&var, 1, MPI_INT, rank - 1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

		var = var + 10 * rank;
		MPI_Send(&var, 1, MPI_INT, rank - 1, 0, MPI_COMM_WORLD);
	}

	MPI_Finalize();
	return 0;
}