#include<mpi.h>
#include<iostream>

using namespace std;

int main(int argc, char **argv)
{
    int rank, size;
    int num1 = 666;
    int num2 = 999;

    MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);

    if (rank == 0)
    {
        cout << "rank 0, num start = " << num1 << endl;
        // send value
        MPI_Send(&num1, 1, MPI_INT, rank+1, 0, MPI_COMM_WORLD);

        // recev value
        int value = 0;
        MPI_Recv(&value, 1, MPI_INT, rank+1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        cout << "rank 0, receiv value = " << value << endl;
    }
    else
    {
        cout << "rank 1, num start = " << num2 << endl;
        // recev value
        int value = 0;
        MPI_Recv(&value, 1, MPI_INT, rank-1, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        cout << "rank 1, receiv value = " << value << endl;

        // send value
        MPI_Send(&num2, 1, MPI_INT, rank-1, 0, MPI_COMM_WORLD);
    }
    
    MPI_Finalize();
    return 0;
}