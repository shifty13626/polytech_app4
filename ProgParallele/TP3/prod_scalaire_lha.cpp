#include<iostream>
#include <mpi.h>

using namespace std;

int main(int argc, char **argv)
{
    // declaration
    double *tab1_global, *tab1_local;
    double *tab2_global, *tab2_local;
    int rank, size;
    int SIZE;

    // declare thread
    MPI_Init( &argc, &argv);

    // get id of process
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    // first proc
    if (rank == 0)
    {
        SIZE = 32;
        tab1_global = (double *) malloc(SIZE * sizeof(double));
        tab2_global = (double *) malloc(SIZE * sizeof(double));
        // init table
        for (int i=0; i<SIZE; i++)
        {
            tab1_global[i] = i;
            tab2_global[i] = 1;
        }
    }

    // on envoit la taille du tableau a chaque processus
    MPI_Bcast(&SIZE, 1, MPI_INT, 0, MPI_COMM_WORLD);

    // init tab local et envoyer aux process
    tab1_local = (double *) malloc(sizeof(double) * SIZE/size);
    tab2_local = (double *) malloc(sizeof(double) * SIZE/size);
    for (int i=0; i<SIZE; i++)
    {
        tab1_local[i] = i;
        tab2_local[i] = 1;
    }
    MPI_Scatter(&tab1_global[0], SIZE/size, MPI_DOUBLE, &tab1_local[0],
        SIZE/size, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Scatter(&tab2_global[0], SIZE/size, MPI_DOUBLE, &tab2_local[0],
        SIZE/size, MPI_DOUBLE, 0, MPI_COMM_WORLD);


    // calcul du scalaire local avant de renvoyer au processus 0 pour la somme total
    double somme = 0.0;
    for (int i=0; i<SIZE/size; i++)
        somme += tab1_local[i] * tab2_local[i];

    // si process 0
    double *rbuf;
    if (rank == 0)
    {
        rbuf = (double *) malloc(size * sizeof(double));
    }
    MPI_Gather(&somme, 1, MPI_DOUBLE, rbuf, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    if (rank == 0)
    {
        for(int i=0; i<size; i++)
            somme += rbuf[i];
        cout << "produit scalaire = " << somme << endl;
    }

    // on libere la memoire de tous les tableaux
    if (rank == 0)
        free(rbuf);
    free(tab1_local);
    free(tab2_local);
    free(tab1_global);
    free(tab2_global);

    MPI_Finalize();
    return 0;    
}