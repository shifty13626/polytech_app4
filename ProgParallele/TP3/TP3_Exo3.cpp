#include <iostream>

#include "mpi.h"

using namespace std;

float f(float x)
{
	return (4/(1+(x*x)));
}

int main(int argc, char **argv)
{
    int rank, size;
    int n = 200;
    double s = (double)1.0 / n;
    double pi = 0.0f;

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
  
    

    MPI_Finalize();
    return 0;
}
