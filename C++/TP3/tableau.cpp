#include <iostream>
#include <memory>
#include <cassert>

using namespace std;

// define template small table
template<typename T, size_t N>
class small_array
{
    // variables
    T data[N];

    public:
    // constructor default
    small_array() = default;
    // contructor copy
    small_array(small_array const &a) = default;
    // constructor transfert
    small_array(small_array &&) = default;
    // destructor
    ~small_array() = default;

    // operators
    // affectation copy
    small_array &operator=(small_array const &a) = default;
    // affectation transfert
    small_array &operator=(small_array &&) = default;

    // accesseur
    T &operator[] (size_t index)
    {
        // check index valide
        assert(index < N);
        return data[index];
    }
    // accesseur without modification
    T const operator[](size_t index) const
    {
        // check index valide
        assert(index < N);
        return data[index];
    }
    T &at(size_t index)
    {
        if (index > N)
            runtime_error("out-of-range");
        return data[index];
    }
    T const &at(size_t index) const
    {
        if (index > N)
            runtime_error("out-of-range");
        return data[index];
    }
};


// define template big table
template<typename T, size_t N>
class large_array
{
    // attibuts
    unique_ptr<small_array<T, N>> data;

    public:
    // constructor
    large_array(): data(new small_array<T, N>) {}
    large_array(large_array const &la): data(new small_array<T, N>(*la.data)) {}
    large_array(large_array &&) = default;


    // destructor
    ~large_array() = default;

    // operator
    large_array &operator=(large_array const &la)
    {
        large_array u = la;
        data.swap(u.data);
        return *this;
    }
    large_array &operator=(large_array &&) = default;
    T &operator[](size_t index)
    {
        assert(index < N);
        return (*data)[index];
    }
    T const &operator[](size_t index) const
    {
        assert(index < N);
        return (*data)[index];
    }
    T &at(size_t index)
    {
        if (index > N)
            runtime_error("Out of bound");
        return (*data)[index];
    }
    T &at(size_t index) const
    {
        if (index > N)
            runtime_error("Out of bound");
        return (*data)[index];
    }
    // sawp
    void swap(large_array &la)
    {
        data.swap(la.data);
    }
};

int main ()
{
    
    // SMALL TABLE
    small_array <int, 4> t;
    t[2] = 42;
    // contructor transfer
    small_array <int, 4> const u = t;
    
    // display table by opertator at
    for( size_t i = 0; i < 4; ++i)
    {
        cout << "[" << i << "] = " << u[i] << endl;
    }
    
    t[4] = 0;//  assertion  failed!

    

    // BIG TABLE
    // normal constructor
    large_array <int, 1000 * 1000 * 10> la;
    la[2] = 42;
    cout << "la[2] = " << la[2] << endl;
    
    // constructor transfert
    large_array <int, 1000 * 1000 * 10> la2(la), temp;
    cout << "la2[2] = " << la[2] << endl;

    temp.swap(la2);
    for (size_t i = 0; i < 4; ++i) {
    cout << '[' << i << "] = " << temp.at(i) << endl;
  }

    return 0;
}