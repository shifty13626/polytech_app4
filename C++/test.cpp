#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

float remplir()
{ 
    static float i = -1;
    i = i + 0.2;
    return i;
}

void displayVector (vector<float> vec)
{
    for (auto element : vec)
        cout << element << " ";
    cout << endl;
}

int main ()
{
    vector<float> vec (10);

    generate(vec.begin(), vec.end(), remplir);

    displayVector(vec);


    vector <float> v(25), w(v.size());
    float min = -20.f;
    float max = 20.f;
    auto roller = [&min, &max]() { return (min- max)*float(rand())/RAND_MAX + max; };
    
    generate(v.begin(), v.end(), roller);
    displayVector(v);

    cout << endl;
    // vector v * 10 -> w
    transform(v.begin(), v.end(), w.begin(), [](float(x)) { return x*10.f; } );
    displayVector(w);

    return 0;
}
