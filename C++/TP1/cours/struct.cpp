#include <iostream>

using namespace std;

struct fraction
{
    public:
    fraction(int _num, int _denum)
    {
        num = _num;
        denum = _denum;
    }
    int getNum()
    {
        return num;
    }
    int getDenum()
    {
        return denum;
    }

    private:
    int num, denum;
};

int main ()
{
    fraction f(1, 5);
    cout << f.getNum() << " / " << f.getDenum() << endl;
    return 0;
}