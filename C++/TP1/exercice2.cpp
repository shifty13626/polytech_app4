#include <iostream>
#include <memory>
#include <map>
 
using namespace std;

/////////////////////////
/// Unique_ptr permet de faire un pointeur
// classique mais ne reste pas a pointer 
// sur un case vide si celle ci est changé
/////////////////////////

class number
{
    public:
    virtual unique_ptr <number> build(int) = 0;
    // = 0 pour virtual pur
    virtual void display () = 0;
    virtual ~number () = default;
};

// rename unique_ptr<number>
typedef unique_ptr<number> number_ptr;

class integer : public number
{
    private:
    int value;

    public:
    integer() = default;
    integer(int x) : value(x) {};

    public:
    virtual void display()
    {
        cout << "(int)" << value << endl;
    }
    virtual number_ptr build(int x)
    {
        return make_unique<integer>(x);
    }
};

class real : public number
{
    private:
    double value;

    public:
    real() = default;
    real(int x) : value(x) {};

    public:
    virtual void display()
    {
        cout << "(real)" << value << endl;
    }
    virtual number_ptr build(int x)
    {
        return make_unique<real>(real(x));
    }
};

class number_factory
{
    map<string, number_ptr> types;

    public:
    number_factory()
    {
        // add element to map
        types["integer"] = make_unique<integer>(0);
        types["real"] = make_unique<real>(0);
    }

    public:
    number_ptr build(string const &s, int x)
    {
        try
        {
            auto found = types.find(s);
            if (found != types.end())
                return found->second->build(x);
            else
                return nullptr;  
        }
        catch(const std::exception& e)
        {
            cerr << e.what() << '\n';
            return nullptr;
        }   
    }
};

namespace TP1_Exercice2
{
	int main(int argc, char* argv[])
	{

		// create object
		number_ptr inte = integer().build(10);
		inte->display();

		number_ptr rea = real().build(2);
		rea->display();

		number_factory factory{};
		auto i = factory.build("integer", 14);
		auto i2 = factory.build("real", 15);
		i->display();
		i2->display();

		return 0;
	}
}