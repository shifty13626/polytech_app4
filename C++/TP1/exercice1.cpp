#include <iostream>
#include <memory>

using namespace std;

/////////////////////////
/// Unique_ptr permet de faire un pointeur
// classique mais ne reste pas a pointer 
// sur un case vide si celle ci est changé
/////////////////////////

class number
{
    public:
    virtual unique_ptr <number> build(int) = 0;
    // = 0 pour virtual pur
    virtual void display () = 0;
    virtual ~number () = default;
};

// rename unique_ptr<number>
typedef unique_ptr<number> number_ptr;

class integer : public number
{
    private:
    int value;

    public:
    integer() = default;
    integer(int x) : value(x) {};

    public:
    virtual void display()
    {
        cout << "(int)" << value << endl;
    }
    virtual number_ptr build(int x)
    {
        return make_unique<integer>(x);
    }
};

class real : public number
{
    private:
    double value;

    public:
    real() = default;
    real(int x) : value(x) {};

    public:
    virtual void display()
    {
        cout << "(real)" << value << endl;
    }
    virtual number_ptr build(int x)
    {
        return make_unique<real>(real(x));
    }
};

namespace TP1_Exercice1
{
	int main(int argc, char* argv[])
	{
		// create object
		number_ptr inte = integer().build(10);
		inte->display();

		number_ptr rea = real().build(2);
		rea->display();

		return 0;
	}
}