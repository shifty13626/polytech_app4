#include <iostream>
#include <memory>
#include <vector>

class node;
typedef std::shared_ptr<node> node_ptr;

class node : public std::enable_shared_from_this<node> {
  std::string name;
  std::vector<std::weak_ptr<node>> parents;
  std::vector<node_ptr> children;
public:

  void add_child(node_ptr n) {
    children.push_back(n);
    n->parents.push_back(shared_from_this());
  }

  std::vector<node_ptr> get_parents() const {
    std::vector<node_ptr> p;
    for (auto const &n: parents) {
      node_ptr m = n.lock();
      if (m) p.push_back(m);
    }
    return p;
  }

  node(std::string const &n): name(n) {}
  ~node() { std::cout << "Destructed " << name << '\n'; }
};

int main() {
  node_ptr a(new node("a"));
  node_ptr b(new node("b"));
  node_ptr c(new node("c"));
  node_ptr d(new node("d"));
  a->add_child(b);
  a->add_child(c);
  d->add_child(b);
  a.reset();
  std::vector<node_ptr> parents = b->get_parents();
  std::cout << "Number of parents: " << parents.size() << '\n';
  return 0;
}
