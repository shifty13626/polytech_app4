//////////////////////////////
//      HAMEL Ludovic       //
//////////////////////////////

#include <iostream>

using namespace std;

// to fix second parameter on function
template<typename F, typename V>
auto set1st(F f, V const &v)
{
    return [f,v](auto x) { return f(v,x); }; 
}

// to fix first parameter on function
template<typename F, typename V>
auto set2st(F const &f, V v)
{
    return [f, v](auto x) { return f(v,x); };
}


int main ()
{
    // set fonction x*2
    auto fonction1 = set1st([](int x, int y)
    {
        cout << x << " * " << y << " = "
        << x*y << endl;
    }, 2);

    // test 3*2
    fonction1(3);
    
    // display arbitrale value
    auto fonction2 = set2st([] (int x, int y)
    {
        cout << "value = " << x << endl;
    }, 5);
    fonction2(333);
    
    return 0;
}