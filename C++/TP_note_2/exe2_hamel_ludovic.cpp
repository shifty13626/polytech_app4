//////////////////////////////
//      HAMEL Ludovic       //
//////////////////////////////

#include <iostream>
#include <memory>

using namespace std;

struct Node
{
    // constructor
    Node() { cout  << "c’tor" << endl; }
    // destructor
    ~Node() { cout  << "d’tor" << endl; }
    
    weak_ptr <Node> parent;
    shared_ptr <Node> left , right;
};

int main()
{
    // construct node with two children
    auto root = make_shared <Node>();
    root->left = make_shared <Node>();
    root->right = make_shared <Node>();

    // affect parent children
    root->left->parent = root;
    root->right->parent = root;
}

// Le programme affiche bien trois construction
// - Noeud principal
//      - fils gauche
//      - fils droit
// Cependant, les shared_ptr libère la mémoire lorsqu'ils 
// ne sont plus utilisé, ici la réafectation du noeud principal
// sur son propre role (il est déjà parent des fils)
// forme une boucle et donc ne libère pas les ressources.
//
// Dans ce cas, il n'y a pas de destruction c'est pour
// cela que dans la sortie nous avons :
//      c'tor
//      c'tor
//      c'tor


// Après avoir passé l'attribut parent de la struct Node
// le pointeur weak_ptr permet d'acceder à un objet uniquement si il existe
// il bloque donc le système de boucle se qui permet d'avoir la destruction
// des objets que l'on utilise plus
// On a donc la sortie suivante :
//      c'tor
//      c'tor
//      c'tor
//      d'tor
//      d'tor
//      d'tor