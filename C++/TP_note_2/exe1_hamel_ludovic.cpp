//////////////////////////////
//      HAMEL Ludovic       //
//////////////////////////////

#include <iostream>

using namespace std;

class Tclass
{
    public:
    // constructor
    Tclass() { cout << "Constructor empty" << endl; };
    Tclass(Tclass const &tc)
    {
        cout << "Constructor by copy " << this << " construct to : " << &tc <<endl; 
    };

    // destructor
    ~Tclass() { cout << "Destructor" << endl; };

    // operator
    Tclass &operator=(Tclass const &tc) = default;
};
    
Tclass f1() { return Tclass(); };
Tclass f2() { Tclass t; return t; };
void f3(Tclass &t) { t = Tclass(); };

struct Ustruct
{
    Tclass v1 , v2;
    Ustruct(Tclass const &t): v2(t) { v1 = t; }
};


int main ()
{
    // construction de a par constructeur vide
    Tclass a;
    // construction de b par constructeur de copie
    Tclass b = a;
    // construction de c par méthode f1 (constructeur vide)
    Tclass c = f1();
    // construction de d par méthode f2 (constructeur vide)
    Tclass d = f2();
    // passage par opérateur de copie, on dit au constructeur vide de
    // prendre le modele de l'objet d
    f3(d);

    // Construction d'un objet Ustruc, on utilise alors une construction
    // d'objet par copie et une autre par constructeur vide
    // v1 --> par affectation
    // v2 --> par copie 
    Ustruct e(a);

    return 0;
}