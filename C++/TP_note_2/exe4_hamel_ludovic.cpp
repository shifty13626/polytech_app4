//////////////////////////////
//      HAMEL Ludovic       //
//////////////////////////////

#include <cstdio>
#include <string>
#include <iostream>
#include <algorithm>

using namespace std;

class file
{
    private:
    FILE * pFile;
    string fileName;

    public:
    // constructor
    file(string nameFile) : fileName(nameFile) 
    {
        pFile = fopen (nameFile.c_str(), "w+");
        cout << "New file created if it already not exist." << endl;
    };
    // copy
    file(file const &fileObject) = delete;
    // transfert
    file(file &&toTransfert)
    {
        swap(toTransfert);
    };

    // destructor
    ~file()
    {
        fclose (pFile);
        cout << "File closed." << endl;
    }

    // operation
    void write(string const &chaine)
    {
        size_t size = chaine.size();
        fwrite(chaine.c_str(), 1, size, pFile);
    }

    void swap(file &fileObject)
    {
        std::swap(this->pFile, fileObject.pFile);
        std::swap(this->fileName, fileObject.fileName);
    }

    // operator
    file &operator=(file const &fileObject) = delete;
};

int main()
{
    cout << "File manager" << endl;

    file f("test1.txt");
    file g("test2.txt");

    f.swap(g);

    f.write("first  string  for  test1\n");
    
    g.write("first  string  for  test2\n");
    f.write("second  string  for  test1\n");

/*

    file f("test1.txt");
    file g = move(f);
    g.write("Test transfert");
*/
/*
    file test = g;
    test.write("toto");
*/
    
    return 0;
}

// Le programme fonctionne, on a pour résultat :
// test1.txt:
//      first  string  for  test1
//      second  string  for  test1

// test2.txt:
//      first  string  for  test2


// Le constructeur de copie fonctionne mais va ecrire 
// dans le même fichier que l'objet initial, cela peut
// modifier involontairement le contenu du fichier si 
// deux objet courant ecrive dans la même cible.
// Il faudrai faire un constucteur par transfert pour
// supprimer la source de la copie

// En utilsant "delete" le compilateur refuser la création
// du nouvel objet avec la forme : "file test(g);"
// en utilisant aussi "delete" pour l'opérateur d'affectation,
// le programme n'accepte plus aucune copie et protège les fichiers
// "file test = g;" n'est plus accepté.

// Avec la méthode swap : 
//       file f("test1.txt");
//       file g("test2.txt");
//       f.swap(g);
// Les textes sont correctement écrit mais dans les fichiers
// inversé, les tests de text1 sont dans text2 et inversement.

// Le constructeur par transfert fonctionne mais une erreur fatale
// apparait lors de la destruction de l'objet transféré
// -> on essaye de détruire un object qui n'existe plus
// Il faut ajouter une vérification d'existance dans le destructeur.
