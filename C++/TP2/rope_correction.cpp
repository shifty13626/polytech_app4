#include <iostream>
#include <memory>
#include <string>

struct TooLarge {};
struct OutOfBounds {};

class rope {
  struct node;
  std::shared_ptr<node const> ptr;
  rope(node const *p): ptr(std::shared_ptr<node const>(p)) {}
public:
  std::size_t size() const;
  rope() = default;
  rope(std::string const &s);
  rope(rope const &l, std::string const &s, rope const &r);
  rope operator+(rope const &r) const;
  rope substr(std::size_t b, std::size_t e) const;
  friend std::ostream &operator<<(std::ostream &out, rope const &r);
};

std::size_t checked_add(std::size_t x, std::size_t y) {
  std::size_t z = x + y;
  if (z < x) throw TooLarge();
  return z;
}

struct rope::node {
  rope left;
  std::string content;
  rope right;
  size_t sz;
  node(std::string const &s):
    content(s), sz(s.size()) {}
  node(rope const &l, std::string const &s, rope const &r):
    left(l), content(s), right(r),
    sz(checked_add(checked_add(l.size(), s.size()), r.size()))
  {}
};

std::size_t rope::size() const {
  if (ptr) return ptr->sz; else return 0;
}

rope::rope(std::string const &s):
  ptr(new node(s)) {}

rope::rope(rope const &l, std::string const &s, rope const &r):
  ptr(new node(l, s, r)) {}

rope rope::operator+(rope const &r) const {
  if (!ptr) return r;
  if (!r.ptr) return *this;
  return rope(new node(*this, std::string(), r));
}

rope rope::substr(std::size_t b, std::size_t l) const {
  if (!l) return rope();
  if (!ptr) throw OutOfBounds();
  node const &n = *ptr;
  if (b == 0 && l == n.sz) return *this;
  if (b >= n.sz || l > n.sz - b) throw OutOfBounds();
  std::size_t lsz = n.left.size();
  std::size_t csz = n.content.size();
  rope r;
  if (b < lsz)
    if (b + l <= lsz) return n.left.substr(b, l);
    else { r = n.left.substr(b, lsz - b); l -= lsz - b; b = 0; }
  else b -= lsz;
  if (b < csz)
    if (b + l <= csz) return r + rope(n.content.substr(b, l));
    else { r = r + rope(n.content.substr(b, csz - b)); l -= csz - b; b = 0; }
  else b -= csz;
  return r + n.right.substr(b, l);
}

std::ostream &operator<<(std::ostream &out, rope const &r) {
  if (r.ptr) out << r.ptr->left << r.ptr->content << r.ptr->right;
  return out;
};

rope ex1() {
  rope de("de");
  rope f("f");
  rope c(de, "c", f);
  rope g("g");
  rope nil(c, "", g);
  rope ab(c, "ab", nil);
  return ab;
}

int main() {
  rope e1 = ex1();
  std::cout << e1 << '\n';
  rope r("abc");
  for (int i = 0; i < 5; ++i) r = r + r;
  std::cout << r << '\n';
  try {
    while (true) r = r + r;
  } catch(TooLarge &) {
    std::cout << "max size: " << r.size() << '\n';
  }
  std::cout << e1.substr(3, 4) << '\n';
  std::cout << r.substr(1000, 1000000000).size() << '\n';
}
