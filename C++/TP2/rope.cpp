#include <iostream>
#include <string>
#include <memory>

using namespace std;

class rope
{
    // attributs class
    public:
        struct node;
        node *ptr;
        rope(node * ptr) : ptr(ptr){};

        // constructor
        public:
        rope() = default;
        rope(string const &s);      // by string
        rope(rope const &l, string const &s, rope const &r);   // rope, string, rope
        rope(rope const &r, string const &s);

        // operator
        rope operator+(rope const &r) const;
        rope operator+(string const &s) const;
};

struct rope::node
{
    // attibuts
    string content;
    rope left;
    rope right;
    
    // constructor
    public:
    node() = default;
    node(string const &content) : content(content), left(), right(){};
    node(rope const &left, string const &content, rope const &right):
        left(left), content(content), right(right) {};
    node(rope const &r, string const &s):
        left(r), content(s), right() {};
};

// output operator
ostream & operator<<(ostream &out, rope const &r){
    if(r.ptr){
        out << r.ptr->left << r.ptr->content << r.ptr->right;
    }
    // else{
    //     out << "NULL";
    // }
    return out;
}

// Addition operator
rope rope::operator+(rope const &r) const
{
    if(!ptr)
        return r;
    if(!r.ptr)
        return *this;

    return rope(new node(*this, string(), r));
}

rope rope::operator+(string const &s) const
{
    auto actualNode = *ptr;
    return rope(new node(*this, actualNode.content + s));
}

int main()
{
    rope r{};
    cout << r << endl;
    rope::node n {"babar"};
    rope r2{&n};
    cout << "Tree r2 : " << r2 << endl;

    // contruct tree exercice 1
    rope::node de {"de"};
    rope::node f {"f"};
    rope::node c {rope(&de), "c", rope(&f)};
    rope::node g {"g"};
    rope::node v {rope(&c), "", rope(&g)};
    rope::node ab {rope(&c), "ab", rope(&v)};
    
    rope exo1 = rope(&ab);

    cout << "Tree exercice 1 : " << exo1 << endl;
    // addition of two rope
    auto testRopeAdd = r2 + exo1;
    cout << "Addition tree : " << testRopeAdd << endl;

    // echec
    auto testAddNodeString = exo1 +" " + "toto";
    cout << "Addition tree + chaine : " << testAddNodeString << endl;

    // destroy test
    cout << "r2 after destroy = " << r2 << endl;

    return 0;
}
