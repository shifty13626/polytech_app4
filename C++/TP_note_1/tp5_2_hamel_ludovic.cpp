////////////////////////////////
// 		HAMEL LUDOVIC		  //
////////////////////////////////

/*

L'objectif est d'écrire une hiérarchie de classe de matrice: matrice dense, triangulaire sup, diagonale.

1. Écrire une classe matrix_t_ abstraite avec comme interface
	- operator[](i, j) virtuelle pure
	- trace() qui renvoit la somme des éléments sur la diagonale
	- print() qui affiche la matrice dans le terminale
	=> trace() et print() sont à implémenter dans matrix_t_
OK

2. Écrire une classe matrix_dense qui hérite de matrice_t_ et implémente operator(i,j)
OK
3. Écrire une classe matrix_triangulaire_sup qui hérite de matrice_t_ et implémente operator(i,j)
OK
4. Écrire une classe matrix_diagonal qui hérite de matrice_t_ et implémente operator(i,j)
OK
5. En utilisant std::chrono, effetuer des mesures de performances de l'implémentation de trace() et print() 
dans le cas des matrices diagonales et triangulaires pour des tailles de matrices croissantes. Commenter les résultats
obtenus.

	Triangulaire : - Print() :  matrice 4*2 : 5590 microseconds
								matrice 8*4 : 9070 microseconds (1.6 fois plus long que 4*2)
								matrice 10*6 : 11666 microseconds

					- Trace :   matrice 4*2 : 637 microseconds
								matrice 8*4 : 817 microseconds
								matrice 10*6 : 816 microseconds

	La fonction print et trace prenne le meme temps car elles ont toutes les deux la même structure, il s'agit d'une double boucle for
	imbriquée. Etant donné qu'elle travail sur le même vecteur il y a le même temps de parcourt.

	Diagonale : - Print() : matrice 2*2 : 1646 microseconds
							matrice 5*3 : 8576 microseconds (5x plus long que 2*2)
							matrice 6*10 : 18644 microseconds (11x plus long que 2*2)

				- Trace() : matrice 2*2 : 1355 microseconds
							matrice 5*3 : 671 microseconds (2x plus rapide que 2*2)
							matrice 6*10 : 732 microseconds (1.8x plus rapide que 2*2)

	Il est normal que le temps d'affichage soit croissant en fonction de la taille de la matrice car le vecteur est de plus en plus grand
	et donc plus long a parcourir

	On remarque que le temps d'exécution pour la matrice triangle est plus important que il y a plus d'élément (les nombres au dessus de la diagonale)
	pour acceder à ces valeurs, l'operateur () fait des opérations supplémentaire.
	Dans le cas de la matrice diagonale on ne fait des calculs pour l'acces que pour les valeurs qui se trouve sur la diagonale.

	Le temps de traitement est donc moins important pour les matrices diagonale qu'une matrice triangle suppérieur pour une matrice de taille équivalente.


6. Rendre trace() et print() virtuelle dans matrix_t_ et les ré-implémenter pour les classes filles

7. Discuter si operator()(i,j) a du sens pour "écrire" dans une matrice non dense ?

	- Pour les matrice triangulaire suppérieur cette opération est encore importante car les données ne sont pas que sur la diagonale, il faut aussi pouvoir récuperer
	et écrire dans les cases supérieurs. Il serai possible de remplir le vecteur de façon différentes en faisant on opérateur d'affectation par transfert.
	Un object vector sera donc compier dans le vecteur de notre objet matrice puis détruit automatiquement.

	- L'opérateur (i,j) n'est plus essentiel pour les matrices diagonales, on pourrai créer un opérateur (i) pour entrer des valeurs.
	Ainsi la commande ressemblerai à : matrice(numLigne) = value;
	La matrice diagonal ne stock des valeurs que pour la diagonale, l'indice de la ligne servira donc à placer cette valeur au bon endroit dans le vecteur.

8. Implémenter une fonction virtuelle add qui prend deux matrices de types quelconques et qui renvoit une matrice
du bon type contenant la somme des éléments des deux matrices. Comment résoudre la problème de connaissance des types 
des deux matrices d'entrées pour en déduire le type de sortie ? Quel outil du standard pouvez vous utiliser pour renvoyer
un objet polymorphe depuis cette fonction membre ?

*/

#include <iostream>
#include <vector>
#include <memory>
#include <chrono>
#include <ctime>
#include <numeric>

using namespace std;

template<typename T>
class matrix_t_
{
	public:
	size_t row, column;

	public:
	// constructor
	matrix_t_(size_t r, size_t c)
	{
		row = r;
		column = c;
	};
	matrix_t_(size_t size) : row(size), column(size) {};

	// destructor
	~matrix_t_() = default;

	public:
	// operator (x, y)
	virtual T &operator()(const size_t &line, const size_t &column) = 0;

	// operation
	public:
	virtual T trace()
	{
		T sum = {};
		for (size_t i=0; i<row; i++)
		{
			for(size_t j=0; j<column; j++)
			{
				if (i == j)
					sum += (*this)(i, j);
			}
		}
		return sum;
	}

	virtual void print()
	{
		for (size_t i=0; i<row; i++)
		{
			for(size_t j=0; j<column; j++)
				cout << "(" << i << "," << j << ") = " << (*this)(i, j) << "\t";
			cout << endl;
		}
	}
};

template<typename T>
class matrix_dense : public matrix_t_<T>
{
	// vector data
	vector<T> vec;

	public:
	// constructor
	matrix_dense(const size_t &x, const size_t &y) : matrix_t_<T> (x, y)
	{
		vec = vector<T>(x * y);
	};
	matrix_dense(const size_t &size) : matrix_t_<T>(size)
	{
		vec = vector<T>(size*size);
	};

	// destructor
	~matrix_dense() = default;

	public:
	// opertator (x, y)
	T &operator()(const size_t &line, const size_t &c) override
	{
		if (line >= 0 && line <= this->row && c >= 0 && c <= this->column)
			return vec.at(line * this->column + c);
		else
			throw out_of_range("Index out of range");	
	}

	virtual T trace() override
	{
		T sum = {};
		for (size_t i=0; i<vec.size(); i = i +this->column +1)
			sum += vec.at(i);

		return sum;
	}

	virtual void print() override
	{
		size_t cpt = 0;
		for(auto element : vec)
		{
			if ((cpt%this->column) == 0)
				cout << endl;
			cout << element << "\t";
			cpt++;
		}
		cout << endl;
	}
};

template<typename T>
class matrix_triangulaire_sup : public matrix_t_<T>
{
	// vector data
	vector<T> vec;
	T defaultValues;

	public:
	// constructor
	matrix_triangulaire_sup(const size_t &x, const size_t &y) : matrix_t_<T> (x, y)
	{
		int sizeVec = 0;
		for(size_t i = 0; i < x && (y-i)>0; i++)
			sizeVec += y - i;

		vec = vector<T>(sizeVec);
		defaultValues = 0;
	};

	matrix_triangulaire_sup(const size_t &size) : matrix_t_<T>(size)
	{
		vec = vector<int> ((size * (size+1)) / 2);
		defaultValues = 0;
	};


	// destructor
	~matrix_triangulaire_sup() = default;

	public:
	// opertator (x, y)
	T &operator()(const size_t &line, const size_t &column) override
	{
		if (line >= 0 && line <= this->row && column >= 0 && column <= this->column)
		{
			if (line > column)
				return defaultValues;
			else
				return vec.at (column + line * this->column - (line * (line+1)) / 2);
		}
		else
			throw out_of_range("Index out of range");
	}

	virtual T trace() override
	{
		T sum = {};
		for (size_t line=0, column=0; line<min(this->row, this->column); column+=this->column-line, line++)
			sum += vec.at(column);
		return sum;
	}

	virtual void print() override
	{
		for (size_t i=0; i<this->row; i++)
		{
			for(size_t j=0; j<this->column; j++)
			{
				if (i <= j)
					cout << (*this)(i, j) << "\t";
				else
					cout << defaultValues << "\t";
			}
			cout << endl;
		}
		cout << endl;
	}
};

template<typename T>
class matrix_diagonal : public matrix_t_<T>
{
	// vector data
	vector<T> vec;
	T defaultValues;

	public:
	// constructor
	matrix_diagonal(const size_t &x, const size_t &y) : matrix_t_<T> (x, y)
	{
		auto sizeVector = min(x, y);
		vec = vector<T>(sizeVector);
		defaultValues = 0;
	};
	matrix_diagonal(const size_t &size) : matrix_t_<T> (size)
	{
		vec = vector<T>(size);
		defaultValues = 0;
	};

	// destructor
	~matrix_diagonal() = default;

	public:
	// opertator (x, y)
	T &operator()(const size_t &line, const size_t &c) override
	{
		if (line >= 0 && line <= this->row && c >= 0 && c <= this->column)
		{
			if (line != c)
				return defaultValues;
			else
				return vec.at(line);
		}
		else
			throw out_of_range("Index out of range");
		
	}

	virtual T trace() override
	{
		return accumulate(vec.begin(), vec.end(), 0);
	}

	virtual void print() override
	{
		for (size_t i=0; i<this->row; i++)
		{
			for(size_t j=0; j<this->column; j++)
			{
				if(i == j)
					cout << vec.at(i) << "\t";
				else
					cout << defaultValues << "\t";
			}
			cout << endl;
		}
		cout << endl;
	}
};


int main()
{
	cout << "Matrice dense" << endl;
	// declare matrix_dense
	matrix_dense<int> dense = {4, 3};
	// init
	dense(0, 0) = 0;
	dense(0, 1) = 1;
	dense(0, 2) = 2;

	dense(1, 0) = 3;
	dense(1, 1) = 4;
	dense(1, 2) = 5;
	
	dense(2, 0) = 6;
	dense(2, 1) = 7;
	dense(2, 2) = 8;

	dense(3, 0) = 9;
	dense(3, 1) = 10;
	dense(3, 2) = 11;

	// display
	chrono::time_point<chrono::system_clock> startPrintDense, endPrintDense, startTraceDense, endTraceDense;
    startPrintDense = chrono::system_clock::now();
	dense.print();
	endPrintDense = chrono::system_clock::now();
    int timePrintDense = chrono::duration_cast<chrono::microseconds> (endPrintDense - startPrintDense).count();
	cout << "Time print matrixDense : " << timePrintDense << " microseconds" << endl;

	// trace
	startTraceDense = chrono::system_clock::now();
	cout << "Trace = " << dense.trace() << endl;
	endTraceDense = chrono::system_clock::now();
    int timeTraceDense = chrono::duration_cast<chrono::microseconds> (endTraceDense - startTraceDense).count();
	cout << "Time trace matrixDense : " << timeTraceDense << " microseconds" << endl;

	cout << endl;
	cout << endl;

	cout << "Matrice triangle sup" << endl;
	// declare triangle sup
	matrix_triangulaire_sup <int> triangleSup = {2, 3};
	// init
	triangleSup(0, 0) = 0;
	triangleSup(0, 1) = 1;
	triangleSup(0, 2) = 2;

	triangleSup(1, 1) = 4;
	triangleSup(1, 2) = 5;

	cout << "triangle supperior create" << endl;

	// display
	chrono::time_point<chrono::system_clock> startPrintTriang, endPrintTriang, startTraceTriang, endTraceTriang;
    startPrintTriang = chrono::system_clock::now();
	triangleSup.print();
	endPrintTriang = chrono::system_clock::now();
    int timePrintTriang = chrono::duration_cast<chrono::microseconds> (endPrintTriang - startPrintTriang).count();
	cout << "Time print matrixTriang : " << timePrintTriang << " microseconds" << endl;

	// trace
	startTraceTriang = chrono::system_clock::now();
	cout << "Trace = " << triangleSup.trace() << endl;
	endTraceTriang = chrono::system_clock::now();
    int timeTraceTriang = chrono::duration_cast<chrono::microseconds> (endTraceTriang - startTraceTriang).count();
	cout << "Time trace matrixTriang : " << timeTraceTriang << " microseconds" << endl;

	cout << endl;
	cout << endl;

	cout << "Matrice diagonale" << endl;
	// declare diagonale
	matrix_diagonal<int> diagonale = {6};
	// init
	diagonale(0, 0) = 1;
	diagonale(1, 1) = 1;
	diagonale(2, 2) = 1;
	diagonale(3, 3) = 1;
	diagonale(4, 4) = 1;
	diagonale(5, 5) = 1;

	// display
	chrono::time_point<chrono::system_clock> startPrintDiag, endPrintDiag, startTraceDiag, endTraceDiag;
    startPrintDiag = chrono::system_clock::now();
	diagonale.print();
	endPrintDiag = chrono::system_clock::now();
    int timePrintDiag = chrono::duration_cast<chrono::microseconds> (endPrintDiag - startPrintDiag).count();
	cout << "Time print matrixDiag : " << timePrintDiag << " microseconds" << endl;

	// trance
	startTraceDiag = chrono::system_clock::now();
	cout << "Trace = " << diagonale.trace() << endl;
	endTraceDiag = chrono::system_clock::now();
    int timeTraceDiag = chrono::duration_cast<chrono::microseconds> (endTraceDiag - startTraceDiag).count();
	cout << "Time trace matrixDiag : " << timeTraceDiag << " microseconds" << endl;

	return 0;
}
